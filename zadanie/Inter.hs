module Inter where
--https://github.com/mgrabmueller/TransformersStepByStep/blob/master/pdf/Transformers.pdf


import Control.Monad.Identity
import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Writer
import Data.Maybe
import qualified Data.Map as M
import AbsPawelLigezaGram
import TypeChecker(runTypeChecker)

import System.IO
data Val = VoidVal
    | IntVal Int 
    | BoolVal Bool
    | StringVal String
    deriving (Eq, Ord, Show, Read)

type Loc = Int
type VName = Ident
type FName = Ident
newtype Fun = Fun ([Expr] -> Interpreter Val)

type VEnv = M.Map VName Loc
type FEnv = M.Map FName Fun
type Env = (VEnv, FEnv)
type Store = M.Map Loc Val


--adding error handling
type Eval2 = ExceptT String IO 

--hidding env 
type Eval3 = ReaderT Env Eval2 

--adding store
type Interpreter a = StateT Store Eval3 a



--helper fun

getVarLoc :: VName -> Interpreter Loc
getVarLoc varName@(Ident realName) = do
    store <- get
    (vEnv, fEnv) <- ask
    case M.lookup varName vEnv of
        Nothing -> throwError ("ERROR: nie istnieje zmienna: " ++ realName)
        Just loc -> return loc 

getVarVal :: VName -> Interpreter Val
getVarVal varName = do
    store <- get
    loc <- getVarLoc varName
    case M.lookup loc store of
        Nothing -> throwError "ERROR: nie istnieje taka lokacja"
        Just val -> return val

--expr evaluations

onlyInt :: Expr -> Interpreter Int
onlyInt expr = do
    val <- evalExp expr
    case val of 
        IntVal x -> return x
        _ -> throwError "ERROR: typ musi byc intem"

onlyBool :: Expr -> Interpreter Bool
onlyBool expr = do 
    val <- evalExp expr
    case val of
        BoolVal x -> return x
        _ -> throwError "ERROR: typ musi byc boolem"

evalExp :: Expr -> Interpreter Val

evalExp (EInt num) = return (IntVal (fromInteger num))
evalExp (ETrue) = return (BoolVal True)
evalExp (EFalse) = return (BoolVal False)
evalExp (EString str) = return (StringVal  str) -- check this


--errory powinien sprawdzic
evalExp (EVar varName@(Ident realName)) = do
    store <- get
    (vEnv, fEnv) <- ask
    case M.lookup varName vEnv of
        Nothing -> throwError ("ERROR: zmienna " ++ realName ++ " nie istnieje")
        Just loc -> case M.lookup loc store of
            Nothing -> throwError "ERROR:  nie ma lokacji"
            Just val -> return val

evalExp (ENeg expr) = do
    val <- onlyInt expr
    return (IntVal((-1) * val))

evalExp (EOr expr1 expr2) = do
    val1 <- onlyBool expr1
    val2 <- onlyBool expr2
    return (BoolVal(val1 || val2))
    

evalExp (EAnd expr1 expr2) = do
    val1 <- onlyBool expr1
    val2 <- onlyBool expr2
    return (BoolVal(val1 && val2))


evalExp (ENot1 expr) = do
    val <- onlyBool expr
    return (BoolVal (not val))

evalExp (ENot2 expr) = do
    val <- onlyBool expr
    return (BoolVal (not val))

evalExp (EEq expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    return (BoolVal (val1 == val2))

evalExp (ENEq expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    return (BoolVal (val1 /= val2))

evalExp (EGq expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    return (BoolVal (val1 > val2))


evalExp (ELq expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    return ( BoolVal (val1 < val2))


evalExp (EGeq expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    return (BoolVal (val1 >= val2))


evalExp (ELeq expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    return (BoolVal (val1 <= val2))

evalExp (EAdd expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    return (IntVal(val1 + val2))

evalExp (ESub expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    return (IntVal(val1 - val2))

evalExp (EMul expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    return (IntVal(val1 * val2))

evalExp (EDiv expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    case val2 of
        0 -> throwError "ERROR: dzielenie przez 0"
        _ -> return (IntVal(val1 `div` val2))

evalExp (EMod expr1 expr2) = do
    val1 <- onlyInt expr1
    val2 <- onlyInt expr2
    case val2 of
        0 -> throwError "ERROR: modulo 0"
        _ -> return (IntVal(val1 `mod` val2))

evalExp (EApp funName@(Ident realName) exprs) = do
    store <- get
    (vEnv, fEnv) <- ask
    case M.lookup funName fEnv of
        Nothing -> throwError ("ERROR: funkcja " ++ realName ++ " nie istnieje")
        Just (Fun fun) -> fun exprs 



--statements eval
evalStmt :: Stmt -> Interpreter Env


evalStmt (SAddAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    val1 <- onlyInt expr
    val2 <- getVarVal varName
    case M.lookup varName vEnv of
        Nothing -> throwError ("ERROR: += , zmienna " ++ realName ++ " nie istnieje")
        Just loc ->  case val2 of
            IntVal x -> modify (M.insert loc (IntVal (val1 + x))) >> return (vEnv, fEnv)
            _ -> throwError "ERROR:  mozna dodawac tylko zmienne typu int"


evalStmt (SAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    val <- evalExp expr
    case M.lookup varName vEnv of
        Nothing -> throwError ("ERROR: = , zmienna " ++ realName ++ " nie istnieje")
        Just loc -> modify (M.insert loc val) >> return (vEnv, fEnv)

evalStmt (SSubAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    val1 <- onlyInt expr
    val2 <- getVarVal varName
    case M.lookup varName vEnv of
        Nothing -> throwError ("ERROR: -= , zmienna " ++ realName ++ " nie istnieje")
        Just loc ->  case val2 of
            IntVal x -> modify (M.insert loc (IntVal (val1 - x))) >> return (vEnv, fEnv)
            _ -> throwError  "ERROR:  mozna odejmowac tylko zmienne typu int"


evalStmt (SMulAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    val1 <- onlyInt expr
    val2 <- getVarVal varName
    case M.lookup varName vEnv of
        Nothing -> throwError ("ERROR: *= , zmienna " ++ realName ++ " nie istnieje")
        Just loc ->  case val2 of
            IntVal x -> modify (M.insert loc (IntVal (val1 * x))) >> return (vEnv, fEnv)
            _ -> throwError "ERROR:  mozna mnozyc tylko zmienne typu int"

evalStmt (SDivAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    val1 <- onlyInt expr
    val2 <- getVarVal varName
    case M.lookup varName vEnv of
        Nothing -> throwError ("ERROR: /= , zmienna " ++ realName ++ " nie istnieje")
        Just loc ->  case val2 of
            IntVal x -> case val1 of 
                0 ->  throwError "ERROR:  nie dziel przez 0"
                _ -> modify (M.insert loc (IntVal (x `div` val1))) >> return (vEnv, fEnv) 
            _ -> throwError "ERROR:  mozna dzielic tylko zmienne typu int"

evalStmt (SModAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    val1 <- onlyInt expr
    val2 <- getVarVal varName
    case M.lookup varName vEnv of
        Nothing -> throwError ("ERROR: &= , zmienna " ++ realName ++ " nie istnieje")
        Just loc ->  case val2 of
            IntVal x -> case val1 of 
                0 ->  throwError "ERROR:  nie modulo przez 0"
                _ -> modify (M.insert loc (IntVal (x `mod` val1))) >> return (vEnv, fEnv)  
            _ -> throwError "ERROR:  mozna modulo tylko zmienne typu int"

evalStmt SEmpty = ask


evalStmt (SPrint expr) = do
    (vEnv, fEnv) <- ask
    val <- evalExp expr
    case val of
        StringVal str -> lift $ lift $ lift $ putStrLn str >> return(vEnv, fEnv)
        IntVal x -> lift $ lift $ lift $ putStrLn (show x) >> return(vEnv, fEnv)
        BoolVal x -> lift $ lift $ lift $ putStrLn (show x) >> return(vEnv, fEnv)
        VoidVal -> return (vEnv, fEnv)

evalStmt (SWhile expr (Block stmts )) = do
    (vEnv, fEnv) <- ask
    cond <- onlyBool expr
    if cond then do
        evalStmts stmts
        evalStmt (SWhile expr (Block stmts ))
    else do
        return (vEnv, fEnv)



evalStmt (SIfElse expr (Block stmts1) elifs (Block stmts2)) = do
    (vEnv, fEnv) <- ask
    cond <- onlyBool expr
    if cond then do
        evalStmts stmts1
    else do
        cond2 <- evalElifs elifs
        if cond2 then do
            return (vEnv, fEnv)
        else do
            evalStmts stmts2

evalStmt (SBlock  (Block stmts)) = do
  --  lift $ lift $ lift $ putStrLn "gowno"
    env <-ask
    local (const env) (evalStmts stmts)
    ask
   
    
    

evalStmt (SIf expr (Block stmts) elifs) = do
    cond <- onlyBool expr
    if cond then do
        evalStmts stmts
    else do
        _ <- evalElifs elifs
        ask



evalStmt(SDecl (Decl t i)) = do
   
    (vEnv, fEnv) <- ask
    tempL <- newLoc
    tempVal <- case i of
        NoInit _ -> defVal t
        Init _ expr  -> evalExp expr
    let tempVarName = case i of
            NoInit  varName -> varName
            Init varName _  -> varName
    modify (M.insert tempL tempVal)
    return (M.insert tempVarName tempL vEnv, fEnv)


evalStmt (SExp expr) = do
    _ <- evalExp expr
    ask

evalStmt(SVoidRet) = ask
evalStmt(SRet expr) = do
    _ <- evalExp expr
    ask
    
evalStmts [] = ask
evalStmts (x : xs) = do
    env <- ask
    envT <- local(const env) (evalStmt x)
    local (const envT ) (evalStmts xs)
    
    


newLoc :: Interpreter Loc
newLoc = do
    store <- get
    return (if M.null store then 1 else fst (M.findMax store) + 1)

defVal :: Type  -> Interpreter Val
defVal t = case t of
    TInt -> return (IntVal 0)  
    TStr -> return (StringVal "")
    TBool -> return (BoolVal False)
    TVoid -> return VoidVal

evalDecl :: Decl -> Interpreter Env
evalDecl (Decl t i) = do
    (vEnv, fEnv) <- ask
    tempL <- newLoc
    tempVal <- case i of
        NoInit _ -> defVal t
        Init _ expr  -> evalExp expr
    let tempVarName = case i of
            NoInit  varName -> varName
            Init varName _  -> varName
    modify (M.insert tempL tempVal)
    return (M.insert tempVarName tempL vEnv, fEnv)

evalDecls :: [Decl] -> Interpreter Env
evalDecls [] = ask
evalDecls (x : xs) = do
    newEnv <- evalDecl x
    local (const newEnv) (evalDecls xs)




filterStmtWithoutDeclFDeclRet :: Stmt -> Bool
filterStmtWithoutDeclFDeclRet stmt = case stmt of
    SDecl _ -> False
    SFDecl _ -> False
    SVoidRet -> False
    SRet _ -> False
    _ -> True



stmtsToDecls :: [Stmt] -> [Decl]
stmtsToDecls [] = []
stmtsToDecls (x : xs ) = case x of
    SDecl decl -> (decl : (stmtsToDecls xs))
    _ -> (stmtsToDecls xs)

stmtsToStmtsWithoutDeclFDeclRet :: [Stmt] -> [Stmt]
stmtsToStmtsWithoutDeclFDeclRet stmts = filter filterStmtWithoutDeclFDeclRet stmts

filterStmtWithoutDeclFDecl :: Stmt -> Bool
filterStmtWithoutDeclFDecl stmt = case stmt of
    SFDecl _ -> False
    _ -> True
stmtsToStmtsWithoutDeclFDecl :: [Stmt] -> [Stmt]
stmtsToStmtsWithoutDeclFDecl stmts = filter filterStmtWithoutDeclFDecl stmts

checkReturn :: Stmt -> Bool
checkReturn stmt = case stmt of
    SVoidRet -> True
    SRet _ -> True
    _ -> False

evalElif :: Elif -> Interpreter Bool
evalElif (Elif expr (Block stmts)) = do
    cond <- onlyBool expr
    if cond then do
        _ <- evalStmts stmts
        return True
    else do
        return False

evalElifs :: [Elif] -> Interpreter Bool
evalElifs [] = return False
evalElifs (x : xs) = do
    y <- evalElif x
    if y then do
        return True
    else do
        ys <- evalElifs xs
        return ys


isReturn :: Stmt -> Bool
isReturn stmt = case stmt of
    SRet _ -> True
    _ -> False

isVoid :: Type -> Bool
isVoid t = case t of
    TVoid -> True
    _ -> False


evalFDecl :: FDecl -> Interpreter Env
evalFDecl (FDecl t funName args (Block stmts)) = do
    (vEnv, fEnv) <- ask
    let newEnv = (vEnv, M.insert funName (Fun (hfun newEnv)) fEnv)
    return newEnv
    where
        hfun:: Env -> [Expr] -> Interpreter Val
        hfun env params = do
            when (length params /= length args) (throwError "ERROR: nie zgadza sie ilosc paramsow")
            tempEnv <- ask
            let lastTemp = last stmts

            if (checkReturn lastTemp) then do
                
                if ((isReturn lastTemp) && (isVoid t)) then do
                    throwError "ERROR: powinien byc pass zamiast return dla void"
                else do
                    if ((not (isReturn lastTemp)) && (not ((isVoid t)))) then do
                        throwError "ERROR: powienien byc return zamiast pass dla nie-void"    
                    else do
                        args <- local (const tempEnv) (paramsToArgs (zip args params))
                        argEnv <- local (const env) (argsToEnv args)
                        let tempStmts = stmtsToStmtsWithoutDeclFDecl stmts
                        envT <- local (const argEnv) (evalStmts tempStmts)
                        local (const envT) (case lastTemp of 
                                                     SVoidRet -> return VoidVal
                                                     SRet expr ->(evalExp expr))
            else do
                case t of
                    TVoid -> throwError "ERROR: brak pass w funkcji - void"
                    _ ->  throwError "ERROR: brak return w funkcji"
                



        argToEnv ::(Arg, Either Loc Val) -> Interpreter Env
        argToEnv(VArg _ varName, Right val) = do
            (vEnv, fEnv) <- ask
            l <- newLoc
            modify(M.insert l val)
            return(M.insert varName l vEnv, fEnv)
        
        argToEnv(RArg _ varName, Left loc) = do
            (vEnv, fEnv) <- ask
            return(M.insert varName loc vEnv, fEnv)
        
        argsToEnv :: [(Arg, Either Loc Val)] -> Interpreter Env
        argsToEnv [] = ask
        argsToEnv (x : xs) = do
            tempEnv <- argToEnv x
            local (const tempEnv) (argsToEnv xs)
        
        paramToArg :: (Arg, Expr) -> Interpreter(Arg, Either Loc Val)
        paramToArg (VArg t varName, expr) = do
            env <- ask
            val <- evalExp expr
            return (VArg t varName, Right val)
        
        paramToArg (RArg t varName@(Ident realName), expr) = do
            (vEnv, fEnv) <- ask
            case expr of
                EVar varName -> case M.lookup varName vEnv of
                    Nothing-> throwError ("ERROR: nie znaleziono paramsu ref " ++ realName)
                    Just loc -> return (RArg t varName, Left loc)
                _ -> throwError "ERROR: referencja params musi byc przez nazwe zmiennej"
            
        paramsToArgs :: [(Arg, Expr)] -> Interpreter [(Arg, Either Loc Val)]
        paramsToArgs [] = return []
        paramsToArgs (x : xs)  = do
            y <- paramToArg x
            ys <- paramsToArgs xs
            return (y : ys)


evalFDecls :: [FDecl] -> Interpreter Env
evalFDecls [] = ask
evalFDecls (x : xs) = do
    newEnv <- evalFDecl x
    local (const newEnv ) (evalFDecls xs)
    
getDeclFromTopDef:: [TopDef] -> [Decl] 
getDeclFromTopDef [] = []
getDeclFromTopDef (x : xs) = case x of 
    GDecl decl -> [decl] ++ (getDeclFromTopDef xs)
    _ -> (getDeclFromTopDef xs)


getFDeclFromTopDef:: [TopDef] -> [FDecl] 
getFDeclFromTopDef [] = []
getFDeclFromTopDef (x : xs) = case x of 
    FnDecl fDecl -> [fDecl] ++ (getFDeclFromTopDef xs)
    _ -> (getFDeclFromTopDef xs)


initStore :: Store
initStore = M.empty

initEnv :: Env
initEnv = (M.empty, M.empty)

startInterpreter :: Program -> Interpreter ()
startInterpreter (Program topdefs) = do
    --let initStore = M.empty
    modify (const initStore)
    let vDecs = getDeclFromTopDef topdefs
    let fDecs = getFDeclFromTopDef topdefs
    vDecEnv <- local (const initEnv) (evalDecls vDecs)
    vFDecEnv <- local (const vDecEnv) (evalFDecls fDecs)
    when (M.notMember (Ident "main") (snd vFDecEnv))(throwError "ERROR: nie ma funkcji main")
    local (const vFDecEnv) (evalExp (EApp (Ident "main") []))
    return ()

runInterpreter :: Program -> Eval2 ()
runInterpreter program = do
    runReaderT (execStateT (startInterpreter program) M.empty) (M.empty, M.empty)
    return ()

execInterpreter:: Program -> IO()
execInterpreter program = do
    typeCheckerRes <- runExceptT (runTypeChecker program)
    case typeCheckerRes of
        Left error -> hPutStrLn stderr (error)
        Right _ -> do
        interRes <- runExceptT (runInterpreter program)
        case interRes of
            Left error ->  hPutStrLn stderr (error)
            Right io -> return io 