module TypeChecker where
import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State
import qualified Data.Map as M
import System.IO
import AbsPawelLigezaGram
data TVal = VoidTVal
    | IntTVal  
    | BoolTVal 
    | StringTVal 
     


instance Show TVal where
    show VoidTVal = "void"
    show IntTVal = "int"
    show BoolTVal = "boolean"
    show StringTVal = "string"

instance Eq TVal where
    VoidTVal == VoidTVal = True
    IntTVal == IntTVal = True
    BoolTVal == BoolTVal = True
    StringTVal == StringTVal = True
    _ == _ = False

type Loc = Int
type VName = Ident
type FName = Ident
data TArg = TVArg TVal | TRArg TVal

data TFun = TFun [TArg] TVal

type VEnv = M.Map VName Loc
type FEnv = M.Map FName TFun
type Env = (VEnv, FEnv)
type Store = M.Map Loc TVal


--adding error handling
type Eval2 = ExceptT String IO 

--hidding env 
type Eval3 = ReaderT Env Eval2 

--adding store
type TypeChecker a = StateT Store Eval3 a

defTVal :: Type  -> TVal
defTVal t = case t of
    TInt ->  IntTVal  
    TStr ->  StringTVal
    TBool ->  BoolTVal
    TVoid  ->  VoidTVal


checkCond :: Expr -> TypeChecker TVal
checkCond expr = do
    tcond <- checkExp expr
    if tcond == BoolTVal then
        return tcond
    else
        throwError ("TYPE-CHECKER ERROR: warunek instrukcji musi być typu\
            \ boolean, a jest " ++ show tcond)

checkExp :: Expr -> TypeChecker TVal


checkExp (EInt num) = return IntTVal
checkExp (ETrue) = return BoolTVal
checkExp (EFalse) = return BoolTVal
checkExp (EString str) = return  StringTVal


--errory powinien sprawdzic typeczeker
checkExp (EVar varName@(Ident realName)) = do
    store <- get
    (vEnv, fEnv) <- ask
    case M.lookup varName vEnv of
        Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie istnieje")
        Just loc -> case M.lookup loc store of
            Nothing -> throwError "TYPE-CHECKER ERROR: pusta lokacja zmiennej"
            Just tval -> return tval


checkExp (ENeg expr) = do
    tval <- checkExp expr
    if tval == IntTVal then 
        return IntTVal
    else
        throwError "TYPE-CHECKER ERROR: Negacja zastosowana nie do wyrazania\
            \ typu int"

checkExp (EOr expr1 expr2) = do
    tval1 <- checkExp expr1
    
    if tval1 == BoolTVal then do
        tval2 <- checkExp expr2
        if tval2 == BoolTVal then 
            return BoolTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator logiczny or zastosowany nie do\
                \ wyrazenia typu boolean"
    else
        throwError "TYPE-CHECKER ERROR: Operator logiczny or zastosowany nie do\
                \ wyrazenia typu boolean"    
    
    
checkExp (EAnd expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == BoolTVal then do
        tval2 <- checkExp expr2
        if tval2 == BoolTVal then
            return BoolTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator logiczny and zastosowany nie do\
                \ wyrazenia typu boolean"
    else
        throwError "TYPE-CHECKER ERROR: Operator logiczny and zastosowany nie do\
                \ wyrazenia typu boolean" 


checkExp (ENot1 expr) = do
    tval <- checkExp expr
    if tval == BoolTVal then
        return BoolTVal
    else
        throwError "TYPE-CHECKER ERROR: Operator logiczny ! zastosowany nie do\
                \ wyrazenia typu boolean"


checkExp (ENot2 expr) = do
    tval <- checkExp expr
    if tval == BoolTVal then
        return BoolTVal
    else
        throwError "TYPE-CHECKER ERROR: Operator logiczny not zastosowany nie do\
                \ wyrazenia typu boolean"

checkExp (EEq expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return BoolTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator porownania == zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator porownania == zastosowany nie do\
                \ wyrazenia typu int"

checkExp (ENEq expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return BoolTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator porownania != zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator porownania != zastosowany nie do\
                \ wyrazenia typu int"

checkExp (EGq expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return BoolTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator porownania > zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator porownania > zastosowany nie do\
                \ wyrazenia typu int"


checkExp (ELq expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return BoolTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator porownania < zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator porownania < zastosowany nie do\
                \ wyrazenia typu int"


checkExp (EGeq expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return BoolTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator porownania >= zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator porownania >= zastosowany nie do\
                \ wyrazenia typu int"


checkExp (ELeq expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return BoolTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator porownania <= zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator porownania <= zastosowany nie do\
                \ wyrazenia typu int"

checkExp (EAdd expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return IntTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator arytmetyczny + zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator arytmetyczny + zastosowany nie do\
                \ wyrazenia typu int"

checkExp (ESub expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return IntTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator arytmetyczny - zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator arytmetyczny - zastosowany nie do\
                \ wyrazenia typu int"

checkExp (EMul expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return IntTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator arytmetyczny * zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator arytmetyczny * zastosowany nie do\
                \ wyrazenia typu int"

checkExp (EDiv expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return IntTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator arytmetyczny / zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator arytmetyczny / zastosowany nie do\
                \ wyrazenia typu int"

checkExp (EMod expr1 expr2) = do
    tval1 <- checkExp expr1
    if tval1 == IntTVal then do
        tval2 <- checkExp expr2
        if tval2 == IntTVal then
            return IntTVal
        else
            throwError "TYPE-CHECKER ERROR: Operator arytmetyczny % zastosowany nie do\
                \ wyrazenia typu int"
    else
        throwError "TYPE-CHECKER ERROR: Operator arytmetyczny % zastosowany nie do\
                \ wyrazenia typu int"



-- to zrobic jeszcze
checkExp (EApp funName@(Ident realName) exprs) = do
    store <- get
    (vEnv, fEnv) <- ask
    case M.lookup funName fEnv of
        Nothing -> throwError ("TYPE-CHECKER ERROR: funkcja " ++ realName ++ " nie\
            \ istnieje ")
        Just (TFun args returnType) -> do
            if (length args) /= (length exprs) then do
                throwError ("TYPE-CHECKER ERROR: funkcja " ++ realName ++ " wywolana\
                    \ z nieodpowiednia liczba argumentow")
            else do
                _ <- checkArgs (zip args exprs)
                return returnType
                where

                    checkArg :: (TArg, Expr) -> TypeChecker ()
                    checkArg(TVArg t, expr) = do
                        tval <- checkExp expr
                        if tval == t then
                            return ()
                        else
                            throwError ("TYPE-CHECKER ERROR: zmienna typu " ++ show t ++ 
                                                ", a zmienna typu " ++ show tval)
                                            
                    checkArg (TRArg t, (EVar varName@(Ident realName1))) = do
                        store <- get
                        (vEnv, fEnv) <- ask
                        case M.lookup varName vEnv of
                            Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName1 ++ 
                                " przekazana do funkcji " ++ realName ++ " przez referencje nie\
                                    \ istnieje")
                            Just loc -> case M.lookup loc store of
                                Just realType -> do
                                    if realType == t then
                                        return ()
                                    else
                                        throwError ("TYPE-CHECKER ERROR: &zmienna typu " ++ show t ++ 
                                                ", a zmienna typu " ++ show realType)
                                Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName1 ++ " nie\
                                    \ została zaalokowana")

                    checkArg (TRArg _, expr ) = 
                        throwError ("TYPE-CHECKER ERROR: wartość niebędąca zmienną\
                            \ przekazana przez referencje do funkcji " ++ realName)
                    
                    checkArgs :: [(TArg, Expr)] -> TypeChecker ()
                    checkArgs [] = return ()
                    checkArgs(x : xs) = checkArg x >> checkArgs xs


checkStmt :: Stmt -> TypeChecker Env

checkStmt (SAddAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    tval <- checkExp expr
    case M.lookup varName vEnv of
        Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
            \ istnieje")
        Just loc ->  case M.lookup loc store of
            Just realType -> do
                if realType == tval && tval == IntTVal then
                    return (vEnv, fEnv)
                else
                    throwError ("TYPE-CHECKER ERROR: można dodawać tylko zmienne \
                        \ typu int, a próbowano dodać zmienną typu " ++ show tval ++ 
                        " na zmienną typu " ++ show realType)
            Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
                \ została zaalokowana")


checkStmt (SAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    tval <- checkExp expr
    case M.lookup varName vEnv of
        Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
            \ istnieje")
        Just loc ->  case M.lookup loc store of
            Just realType -> do
                if realType == tval then
                    return (vEnv, fEnv)
                else
                    throwError ("TYPE-CHECKER ERROR: do zmiennej typu " ++ show realType ++
                    " próbowano przypisać zmienną typu " ++ show tval)
            Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
                \ została zaalokowana")

checkStmt (SSubAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    tval <- checkExp expr
    case M.lookup varName vEnv of
        Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
            \ istnieje")
        Just loc ->  case M.lookup loc store of
            Just realType -> do
                if realType == tval && tval == IntTVal then
                    return (vEnv, fEnv)
                else
                    throwError ("TYPE-CHECKER ERROR: można odejmować tylko zmienne\
                        \ typu int, a próbowano odjąć zmienną typu " ++ show tval ++ 
                        " od zmiennej typu " ++ show realType)
            Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
            \ została zaalokowana")


checkStmt (SMulAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    tval <- checkExp expr
    case M.lookup varName vEnv of
        Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
            \ istnieje")
        Just loc ->  case M.lookup loc store of
            Just realType -> do
                if realType == tval && tval == IntTVal then
                    return (vEnv, fEnv)
                else
                    throwError ("TYPE-CHECKER ERROR: można mnożyć tylko zmienne\
                        \ typu int, a próbowano mnożyć zmienną typu " ++ show tval ++ 
                        " ze zmienną typu " ++ show realType)
            Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
                \ została zaalokowana")

checkStmt (SDivAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    tval <- checkExp expr
    case M.lookup varName vEnv of
        Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
            \ istnieje")
        Just loc ->  case M.lookup loc store of
            Just realType -> do
                if realType == tval && tval == IntTVal then
                    return (vEnv, fEnv)
                else
                    throwError ("TYPE-CHECKER ERROR: można dzielić tylko zmienne\ 
                        \ typu int, a próbowano dzielić typu " ++ show tval ++ 
                        " ze zmienną typu " ++ show realType)
            Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
                \ została zaalokowana")

checkStmt (SModAss varName@(Ident realName) expr) = do
    store <- get
    (vEnv, fEnv) <- ask
    tval <- checkExp expr
    case M.lookup varName vEnv of
        Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
            \ istnieje")
        Just loc ->  case M.lookup loc store of
            Just realType -> do
                if realType == tval && tval == IntTVal then
                    return (vEnv, fEnv)
                else
                    throwError ("TYPE-CHECKER ERROR: operacja modulo działa tylko na zmiennych\ 
                        \ typu int, a próbowano na typie " ++ show tval ++ 
                        " ze zmienną typu " ++ show realType)
            Nothing -> throwError ("TYPE-CHECKER ERROR: zmienna " ++ realName ++ " nie\
                \ została zaalokowana")

            
checkStmt SEmpty = ask

checkStmt (SPrint expr) = do
    val <- checkExp expr
    if val == VoidTVal then
        throwError ("TYPE-CHECKER ERROR: typ void nie moze zostac wypisany")
    else
        ask




checkStmt (SWhile expr (Block stmts )) = do
    checkCond expr >> checkStmts stmts 






    
checkStmt (SIf expr (Block stmts) elifs) = do
    checkCond expr >> checkStmts stmts >> ask

checkStmt (SIfElse expr (Block stmts1) elifs (Block stmts2)) = do
    checkCond expr >> checkStmts stmts1 >> checkElifs elifs >> checkStmts stmts2 >> ask

checkStmt (SBlock (Block stmts)) = do
    env <- ask
    local (const env) (checkStmts stmts)
    ask


checkStmt(SDecl (Decl t i)) = do
   
    (vEnv, fEnv) <- ask
    tempL <- newLoc
    tempVal <- case i of
        NoInit _ -> return (defTVal t)
        Init _ expr  -> do
            tval <- checkExp expr
            if tval == (defTVal t) then
                return tval
            else
                throwError ("TYPE-CHECKER ERROR: probowano na zmienna typu " ++ show (defTVal t) 
                    ++ " przypisac wartosc typu " ++ show tval)
    let tempVarName = case i of
            NoInit  varName -> varName
            Init varName _  -> varName
    
    modify (M.insert tempL tempVal)
    return (M.insert tempVarName tempL vEnv, fEnv)




checkStmt (SExp expr) = do
    checkExp expr >> ask

checkStmt(SVoidRet) = ask
checkStmt(SRet expr) = do
    checkExp expr >> ask

checkStmts [] = ask
checkStmts (x : xs) = do
    env <- ask
    envT <- local(const env) (checkStmt x)
    local (const envT ) (checkStmts xs)


newLoc :: TypeChecker Loc
newLoc = do
    store <- get
    return (if M.null store then 1 else fst (M.findMax store) + 1)

checkElif :: Elif -> TypeChecker Env
checkElif (Elif expr (Block stmts)) = do
    checkCond expr >> checkStmts stmts 

checkElifs :: [Elif] -> TypeChecker Env
checkElifs [] = ask
checkElifs (x : xs) = do
    checkElif x >> checkElifs xs >> ask

checkDecl :: Decl -> TypeChecker Env
checkDecl (Decl t i) = do
    (vEnv, fEnv) <- ask
    tempL <- newLoc
    tempVal <- case i of
        NoInit _ -> return (defTVal t)
        Init _ expr  -> do
            tval <- checkExp expr
            if tval == (defTVal t) then
                return tval
            else
                throwError ("TYPE-CHECKER ERROR: probowano na zmienna typu " ++ show (defTVal t) 
                    ++ " przypisac wartosc typu " ++ show tval)
    let tempVarName = case i of
            NoInit  varName -> varName
            Init varName _  -> varName
    
    modify (M.insert tempL tempVal)
    return (M.insert tempVarName tempL vEnv, fEnv)

checkDecls :: [Decl] -> TypeChecker Env
checkDecls [] = ask
checkDecls (x : xs) = do
    newEnv <- checkDecl x
    local (const newEnv) (checkDecls xs)


filterStmtWithoutDeclFDeclRet :: Stmt -> Bool
filterStmtWithoutDeclFDeclRet stmt = case stmt of
    SDecl _ -> False
    SFDecl _ -> False
    SVoidRet -> False
    SRet _ -> False
    _ -> True



stmtsToDecls :: [Stmt] -> [Decl]
stmtsToDecls [] = []
stmtsToDecls (x : xs ) = case x of
    SDecl decl -> (decl : (stmtsToDecls xs))
    _ -> (stmtsToDecls xs)

stmtsToStmtsWithoutDeclFDeclRet :: [Stmt] -> [Stmt]
stmtsToStmtsWithoutDeclFDeclRet stmts = filter filterStmtWithoutDeclFDeclRet stmts

filterStmtWithoutDeclFDecl :: Stmt -> Bool
filterStmtWithoutDeclFDecl stmt = case stmt of
    SFDecl _ -> False
    _ -> True
stmtsToStmtsWithoutDeclFDecl :: [Stmt] -> [Stmt]
stmtsToStmtsWithoutDeclFDecl stmts = filter filterStmtWithoutDeclFDecl stmts

checkReturn :: Stmt -> Bool
checkReturn stmt = case stmt of
    SVoidRet -> True
    SRet _ -> True
    _ -> False

isReturn :: Stmt -> Bool
isReturn stmt = case stmt of
    SRet _ -> True
    _ -> False

isVoid :: Type -> Bool
isVoid t = case t of
    TVoid -> True
    _ -> False


checkFDecl :: FDecl -> TypeChecker Env
checkFDecl (FDecl t funName@(Ident realName) args (Block stmts)) = do
    (vEnv, fEnv) <- ask
    let funType = defTVal t
    let argsTypes = fmap (\arg -> case arg of
                        VArg tt _ -> TVArg (defTVal tt)
                        RArg tt _ -> TRArg (defTVal tt)
                        ) args

    let newEnv = (vEnv, M.insert funName (TFun argsTypes (defTVal t)) fEnv)
    returnedType <- hfun newEnv

    if returnedType == funType then
        return newEnv
    else
        throwError ("TYPE-CHECKER ERROR: funkcja " ++ show realName ++ " jest typu " ++ 
            show funType ++ " ale zwraca typ " ++ show returnedType)
    where
        hfun:: Env -> TypeChecker TVal
        hfun env  = do
            let lastTemp = last stmts
            
            if (checkReturn lastTemp) then do
                
                if ((isReturn lastTemp) && (isVoid t)) then do
                    throwError ("TYPE-CHECKER ERROR: funkcja " ++ show realName ++ " powinna\
                    \ mieć pass zamiast return dla void")
                else do
                    if ((not (isReturn lastTemp)) && (not ((isVoid t)))) then do
                        throwError ("TYPE-CHECKER ERROR: funkcja " ++ show realName ++ " powinna\
                        \ mieć return zamiast pass dla typu nie będącym void")   
                    else do
                        argEnv <- local (const env) (argsToEnv args)
                        let tempStmts = stmtsToStmtsWithoutDeclFDecl stmts
                        envT <- local (const argEnv) (checkStmts tempStmts)
                        local (const envT) (case lastTemp of 
                                                     SVoidRet -> return VoidTVal
                                                     SRet expr ->(checkExp expr))
            else do
                case t of
                    TVoid -> throwError ("TYPE-CHECKER ERROR: funkcja " ++ show realName ++ " nie\
                    \ posiada pass")
                    _ ->  throwError ("TYPE-CHECKER ERROR: funkcja " ++ show realName ++ " nie\
                    \ posiada return")
                

        argToEnv ::Arg -> TypeChecker Env
        argToEnv(VArg t varName) = do
            (vEnv, fEnv) <- ask
            l <- newLoc
            modify(M.insert l (defTVal t))
            return(M.insert varName l vEnv, fEnv)
        
        argToEnv(RArg t varName) = do
            (vEnv, fEnv) <- ask
            l <- newLoc
            modify(M.insert l (defTVal t))
            return(M.insert varName l vEnv, fEnv)
        
        argsToEnv :: [Arg] -> TypeChecker Env
        argsToEnv [] = ask
        argsToEnv (x : xs) = do
            tempEnv <- argToEnv x
            local (const tempEnv) (argsToEnv xs)
        


checkFDecls :: [FDecl] -> TypeChecker Env
checkFDecls [] = ask
checkFDecls (x : xs) = do
    newEnv <- checkFDecl x
    local (const newEnv ) (checkFDecls xs)

getDeclFromTopDef:: [TopDef] -> [Decl] 
getDeclFromTopDef [] = []
getDeclFromTopDef (x : xs) = case x of 
    GDecl decl -> [decl] ++ (getDeclFromTopDef xs)
    _ -> (getDeclFromTopDef xs)


getFDeclFromTopDef:: [TopDef] -> [FDecl] 
getFDeclFromTopDef [] = []
getFDeclFromTopDef (x : xs) = case x of 
    FnDecl fDecl -> [fDecl] ++ (getFDeclFromTopDef xs)
    _ -> (getFDeclFromTopDef xs)


initStore :: Store
initStore = M.empty

initEnv :: Env
initEnv = (M.empty, M.empty)

startTypeChecker :: Program -> TypeChecker ()
startTypeChecker (Program topdefs) = do
    --let initStore = M.empty
    modify (const initStore)
    let vDecs = getDeclFromTopDef topdefs
    let fDecs = getFDeclFromTopDef topdefs
    vDecEnv <- local (const initEnv) (checkDecls vDecs)
    vFDecEnv <- local (const vDecEnv) (checkFDecls fDecs)
    when (M.notMember (Ident "main") (snd vFDecEnv))(throwError "TYPE-CHECKER ERROR:\
        \ nie ma funkcji main")
    local (const vFDecEnv) (checkExp (EApp (Ident "main") []))
    return ()

runTypeChecker :: Program -> Eval2 ()
runTypeChecker program = do
    runReaderT (execStateT (startTypeChecker program) M.empty) (M.empty, M.empty)
    return ()


    




