BNFC?=/home/students/inf/PUBLIC/MRJP/bin/bnfc
dir=build
all:
	mkdir -p ${dir}
	cd zadanie && ${BNFC} -m pawel_ligeza_gram.cf && make && \
	ghc --make main.hs -odir ../build -hidir ../build -o ../iter

clean:
	rm -rf build
	rm -rf iter
	rm -rf zadanie/AbsPawelLigezaGram.hs zadanie/DocPawelLigezaGram.txt
	rm -rf zadanie/ErrM.hs zadanie/LexPawelLigezaGram.hs zadanie/LexPawelLigezaGram.x
	rm -rf zadanie/ParPawelLigezaGram.hs zadanie/ParPawelLigezaGram.y
	rm -rf zadanie/PrintPawelLigezaGram.hs zadanie/SkelPawelLigezaGram.hs
	rm -rf zadanie/TestPawelLigezaGram.hs zadanie/TestPawelLigezaGram 
	rm -rf zadanie/*.hi zadanie/*.o zadanie/ParPawelLigezaGram.info zadanie/Makefile

